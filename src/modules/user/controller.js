import passport from 'passport';

export const UserController = {};
export default { UserController };

/**
 * Display user login form
 */
UserController.loginForm = (req, res) => {
  res.render('user/views/login');
};

UserController.logout = (req, res) => {
  req.session.destroy();
  return res.redirect('/login');
};

UserController.dashboard = (req, res) => {
  res.render('user/views/dashboard');
};

UserController.login = (req, res, next) => {
  passport.authenticate('local-login', (err, user) => {
    if (err) {
      return next(err);
    }
    req.user = user;
    return next();
  })(req, res, next);
};
